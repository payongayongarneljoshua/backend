###### BACKEND-EXAM ########


To run the project need to run few commands

# TO INSTALL DEPEDENCIES #

[1] npm install 

[2] cp .example.env .env 

[3] npm run migrate-up  

####  UNIT TESTING   ###
npm run build-test

### API ENDPOINT  ####

GET /user/display-users\
NO PARAMS\
-Return all users

GET /user/display-user/:id \
NO PARAMS \
-Return specific user by id 

POST /user/create-user\
PARAMS\
"email" : "email@example.com" , | required\
"username" : "username", | required\
"password" : "12345678", | required\
"firstName" : "firstname", | required\
"lastName" : "lastname", | required\
"address" : "address", | required\
"phone" : "+63 123 456 78", | required\
"postcode" : "1234" | required\

-Return successfully inserted data


PUT /user/update-user\
PARAMS\
"email" : "email@example.com" , | required\
"username" : "username", | required\
"password" : "12345678", | required\
"firstName" : "firstname", | required\
"lastName" : "lastname", | required\
"address" : "address", | required\
"phone" : "+63 123 456 78", | required\
"postcode" : "1234" | required\
-Return successfully updated data




DELETE /user/delete-users\
PARAMS\
usersID : []\
-Return Deleted Users ID


DELETE /user/delete/user/:id\
NO PARAMS\
-Return Deleted User ID





