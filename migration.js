const mysql = require('mysql');
const migration = require('mysql-migrations');
const config = require('./config/mysqli').default

const connection = mysql.createPool(config);

migration.init(connection, __dirname + '/migrations', function() {
  console.log("finished running migrations");
});