const express = require('express');
const UserRouter = express.Router();
const user = require('../controllers/user').default

UserRouter.get('/display-users',async function(req,res) {

    try {
        let request = req.body
        let select = request.select

        const results = await user.select(select);

        res.status(results.code).send(results)
    } catch (error) {
        res.status(error.code).send(error)
    }
})

UserRouter.get('/display-user/:id',async function(req,res) {

    try {
        let request = req.body
        
        let select = req.body.select
        let where = req.body.where
        let id = req.params.id

        const results = await user.select(select,where,id);

        res.status(results.code).send(results)
    } catch (error) {
        res.status(error.code).send(error)
    }
})

UserRouter.post('/create-user',async function(req,res)  {

    try {
        let request = req.body

        const results = await user.insert(request);

        res.status(200).json(results)

    } catch (error) {

        res.status(422).json(error)
        
    }
})

UserRouter.put('/update-user/:id',async function(req,res)  {
    try {

        let request = req.body

        const results = await user.update(request,req.params.id);
        res.status(results.code).json(results)
    } catch (error) {
        res.status(error.code).json(error.error)
        
    }
})

UserRouter.delete('/delete-user/:id',async function(req,res) {

    try {
        const results = await user.delete(req.params.id);

        res.status(results.code).json(results)
    } catch (error) {

        res.status(error.code).json(error)
        
    }
})

UserRouter.delete('/delete-users/',async function(req,res) {

    try {

        let usersID = req.body.usersID

        const results = await user.delete(usersID);

        res.status(results.code).json(results)
    } catch (error) {

        res.status(error.code).json(error)
        
    }
})



exports.default = UserRouter