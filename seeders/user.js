const faker = require('@faker-js/faker')
const mysql = require('mysql');
const config = require('../config/mysqli').default


const connection = mysql.createConnection(config)
console.log('--- Users Seeding --')

var total = 10
var success = 0

function insertData(data,index){
    let keys = Object.keys(data)
    let values = Object.values(data)

    let statement = `INSERT INTO users (${keys.toString()}) VALUES (${values.map(v => {return `'${v.replace(/'/g, "\\'")}'`})})`

    connection.query(statement,(err,result) => {
        if(err){
            console.log(err)
            console.log(index + ' FAILED')
        }

        success++;
        console.log((index == -1) ? 'Superadmin OK': (index + 1) + ' OK')
    })
}


let dummyData = {
    "email" :  'superadmin@admin.com',
    "username" : 'superadmin',
    "password" : faker.fake('{{internet.password}}'),
    "firstName" :'Super',
    "lastName" : 'Admin',
    "address" : faker.fake('{{address.city}}'),
    "phone" : faker.fake('{{phone.phoneNumber}}'),
    "postcode" : faker.fake('{{address.zipCode}}'),
}

console.log('[Superadmin credentials]')
console.log(dummyData)
insertData(dummyData,-1)

for(let i = 0; i < total; i++){

    let dummyData = {
        "email" :  faker.fake('{{internet.email}}'),
        "username" : faker.fake('{{internet.userName}}'),
        "password" : faker.fake('{{internet.password}}'),
        "firstName" :faker.fake('{{name.firstName}}'),
        "lastName" : faker.fake('{{name.lastName}}'),
        "address" : faker.fake('{{address.city}}'),
        "phone" : faker.fake('{{phone.phoneNumber}}'),
        "postcode" : faker.fake('{{address.zipCode}}'),
    }

    insertData(dummyData,i)
}
