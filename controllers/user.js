"use strict";

const validation = require('../validations/user').default
const config = require('../config/mysqli').default

const mysql = require('mysql');



const table = 'users'

const connection = mysql.createConnection(config)

const user = {
    insert :  (data) => {
        return new Promise(async (resolve,reject) => {

            let keys = Object.keys(data)
            let values = Object.values(data)


                    
            let validate = await validation(data)


            if(validate != undefined)
                return reject({
                    code: 422,
                    message: validate
                })

            let statement = `INSERT INTO ${table} (${keys.toString()}) VALUES (${values.map(v => {return `'${v}'`})})`

            connection.query(statement,(err,result) => {
                if(err){
                    return reject(err)
                }

                data['id'] = result.insertId

                return resolve({
                    data: data,
                    code: 200,
                    message: "Successfully Inserted"
                })
            })

           
        }) 
    },
    delete : (id) => {
        return new Promise((resolve,reject) => {

            let statement = ''

            if(Array.isArray(id))
                statement = `DELETE FROM ${table} WHERE id IN (${id.toString()})`
            else
                statement = `DELETE FROM ${table} WHERE id = ${id}`

            connection.query(statement,(err,result) => {

                if(err){
                    return reject({
                        code : 422,
                        message: err
                    })
                }

                return resolve({
                    affectedRows: result.affectedRows,
                    id: id,
                    message: "Successfully deleted",
                    code: 200
                })
            })
        }) 
    },
    update : (data,id) => {
        return new Promise(async (resolve,reject) => {


            let keys = Object.keys(data)
            let values = Object.values(data)

            let validate = await validation(data)


            if(validate != undefined)
                return reject({
                    error: validate,
                    code: 422,
                })


            let setQuery = []

            for(let i = 0; i < keys.length; i++){
                setQuery.push(` ${keys[i]} = '${values[i]}'`)
            }
            
            let statement = `UPDATE ${table} SET ${setQuery.toString()} WHERE id = ${id}`

            connection.query(statement,(err,result) => {
                if(err){
                    return reject({
                        message: err,
                        code: 422
                    })
                }
                
                return resolve({
                    data: data,
                    code: 201,
                    message: "Successfully Updated"
                })
            })
        }) 
    },
    select : async (select = null,id = null) => {

        return new Promise((resolve,reject) => {

            let selectQuery = 'SELECT *'
            let whereQuery = ''

            if(select){
                selectQuery = 'SELECT '
                selectQuery += select.toString()
            }

            if(id){
                whereQuery = `WHERE id = '${id}'` 
            }
            
   
            connection.query(`${selectQuery} FROM ${table} ${whereQuery}`,(err,result) => {
                if(err){
                    return reject({
                        message: err,
                        code: 422
                    })
                }

                return resolve({
                    data: result,
                    code: 201,
                    message: "Successfully fetch"
                })
            })
        })      
    }
}


exports.default = user