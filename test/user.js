const expect = require('chai').expect;

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server/app').default;
const should = chai.should();

chai.use(chaiHttp);


describe('/POST user/create-user', () => {
  it('it should not POST a user without email , username, password firstName , lastName, address, phone, postcode field', (done) => {
      let user = {

      } 
    chai.request(server)
        .post('/user/create-user')
        .send(user)
        .end((err, res) => {
              res.should.have.status(422);
              res.body.should.have.property('code').eql(422);
          done();
        });
  });

  it('it should not POST a user without valid email (testexample.com) field', (done) => {
    let user = {
      "email" : "testexample.com",
      "username" : "username",
      "password" : "password12345678",
      "firstName" : "firstname",
      "lastName" : "lastname",
      "address" : "example address",
      "phone" : "+123456",
      "postcode" : "4400"
  }

  chai.request(server)
      .post('/user/create-user')
      .send(user)
      .end((err, res) => {
            res.should.have.status(422);
            res.body.should.have.property('code').eql(422);
        done();
      });
  });

  it('it should not POST a user without valid password (12345678) field', (done) => {
    let user = {
      "email" : "testexample.com",
      "username" : "username",
      "password" : "password12",
      "firstName" : "firstname",
      "lastName" : "lastname",
      "address" : "example address",
      "phone" : "+123456",
      "postcode" : "4400"
  }

  chai.request(server)
      .post('/user/create-user')
      .send(user)
      .end((err, res) => {
            res.should.have.status(422);
            res.body.should.have.property('code').eql(422);
        done();
      });
  });

  it('it should not POST a user without valid password (1234567891112) field', (done) => {
    let user = {
      "email" : "testexample.com",
      "username" : "username",
      "password" : "1234567891112",
      "firstName" : "firstname",
      "lastName" : "lastname",
      "address" : "example address",
      "phone" : "+123456",
      "postcode" : "4400"
  }

  chai.request(server)
      .post('/user/create-user')
      .send(user)
      .end((err, res) => {
            res.should.have.status(422);
            res.body.should.have.property('code').eql(422);
        done();
      });
  });

  it('it should POST a user email (testy@opmail.com)', (done) => {
    let user = {
      "email" : "testy@opmail.com",
      "username" : "ancela",
      "password" : "1234567011",
      "firstName" : "TEST",
      "lastName" : "TEST",
      "address" : "BLOCK 2 LOT 4",
      "phone" : "+123456",
      "postcode" : "4400"
  }

chai.request(server)
      .post('/user/create-user')
      .send(user)
      .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Successfully Inserted');
        done();
      });
});

  it('it should not POST a user need to be unique email(testy@opmail.com)', (done) => {
    let user = {
      "email" : "testy@opmail.com",
      "username" : "ancela",
      "password" : "1234567011",
      "firstName" : "TEST",
      "lastName" : "TEST",
      "address" : "BLOCK 2 LOT 4",
      "phone" : "+123456",
      "postcode" : "4400"
  }

  chai.request(server)
        .post('/user/create-user')
        .send(user)
        .end((err, res) => {
              res.should.have.status(422);
          done();
        });
  });


});

describe('/GET user/display-users | user/display-user', () => {
  it('it should GET all the users', (done) => {
    chai.request(server)
        .get('/user/display-users')
        .end((err, res) => {
              res.should.have.status(201);
          done();
        });
  });

  it('it should GET all the users with specific fields', (done) => {

    let select = {
      select : ['firstName','lastName']
    }

    chai.request(server)
        .get('/user/display-users')
        .send(select)
        .end((err, res) => {
              res.should.have.status(201);
          done();
        });
  });



  it('it should GET specific user', (done) => {
    chai.request(server)
        .get('/user/display-user/1')
        .send()
        .end((err, res) => {
              res.should.have.status(201);
          done();
        });
  });
});


describe('/PUT user/update-user', () => {
    it('it should not PUT a user without email , username, password firstName , lastName, address, phone, postcode field', (done) => {
      let user = {

      } 
    chai.request(server)
        .put('/user/update-user/9')
        .send(user)
        .end((err, res) => {
              res.should.have.status(422);
              res.body.should.be.a('object');
          done();
        });
  });

  it('it should not PUT a user without valid email (testexample.com) field', (done) => {
    let user = {
      "email" : "testexample.com",
      "username" : "username",
      "password" : "password12345678",
      "firstName" : "firstname",
      "lastName" : "lastname",
      "address" : "example address",
      "phone" : "+123456",
      "postcode" : "4400"
  }

  chai.request(server)
      .put('/user/update-user/1')
      .send(user)
      .end((err, res) => {
            res.should.have.status(422);
            res.body.should.be.a('object');
        done();
      });
  });

  it('it should not POST a user without valid password (12345678) field', (done) => {
    let user = {
      "email" : "testexample.com",
      "username" : "username",
      "password" : "password12",
      "firstName" : "firstname",
      "lastName" : "lastname",
      "address" : "example address",
      "phone" : "+123456",
      "postcode" : "4400"
  }

  chai.request(server)
      .put('/user/update-user/1')
      .send(user)
      .end((err, res) => {
            res.should.have.status(422);
            res.body.should.be.a('object');
        done();
      });
  });

  it('it should not POST a user without valid password (1234567891112) field', (done) => {
    let user = {
      "email" : "testexample.com",
      "username" : "username",
      "password" : "1234567891112",
      "firstName" : "firstname",
      "lastName" : "lastname",
      "address" : "example address",
      "phone" : "+123456",
      "postcode" : "4400"
  }

  chai.request(server)
      .put('/user/update-user/1')
      .send(user)
      .end((err, res) => {
            res.should.have.status(422);
            res.body.should.be.a('object');
        done();
      });
  });

  it('it should not PUT user need to be unique email', (done) => {

    let user = {
      "email" : "unique@test.com",
      "username" : "uniquetest",
      "password" : "1234567011",
      "firstName" : "TEST",
      "lastName" : "TEST",
      "address" : "BLOCK 2 LOT 4",
      "phone" : "+123456",
      "postcode" : "4400"
   }

    chai.request(server)
        .put('/user/update-user/9')
        .send(user)
        .end((err, res) => {
              res.should.have.status(201);
          done();
        });
  });

  it('it should PUT user', (done) => {

    let user = {
      "email" : "unique@test.com",
      "username" : "uniquetest",
      "password" : "1234567011",
      "firstName" : "TEST",
      "lastName" : "TEST",
      "address" : "BLOCK 2 LOT 4",
      "phone" : "+123456",
      "postcode" : "4400"
   }

    chai.request(server)
        .put('/user/update-user/9')
        .send(user)
        .end((err, res) => {
              res.should.have.status(201);
              res.body.should.be.a('object');
          done();
        });
  });
});


describe('/DELETE user/delete-user | user/delete-users', () => {
  it('it should not DELETE the user', (done) => {
    chai.request(server)
        .delete('/user/delete-user/s')
        .end((err, res) => {
              res.should.have.status(422);
          done();
        });
  });

  it('it should not DELETE the user', (done) => {
    chai.request(server)
        .delete('/user/delete-user/20')
        .end((err, res) => {
              res.should.have.status(200);
              res.body.should.have.property('affectedRows').equal(0)
          done();
        });
  });

  
  it('it should DELETE the user', (done) => {
    chai.request(server)
        .delete('/user/delete-user/1')
        .end((err, res) => {
              res.should.have.status(200);
          done();
        });
  });

  it('it should DELETE the users', (done) => {

    let data = {
      usersID : [2,3,4,5]
    }

    chai.request(server)
        .delete('/user/delete-users')
        .send(data)
        .end((err, res) => {
              res.should.have.status(200);
          done();
        });
  });
});



