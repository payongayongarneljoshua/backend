const validate = require("validate.js")


var constraints = {
    firstName: {
      presence: {allowEmpty : false}
    },
    lastName:{
        presence: {allowEmpty : false}
    },
    username:{
        presence: {allowEmpty : false},
        length: {
            maximum: 15,
        }
    },
    password: {
        presence: {allowEmpty : false},
        length: {
            minimum: 10,
            maximum: 15,
        }
    },
    address: {
        presence: {allowEmpty : false}
    },
    phone: {
        presence: {allowEmpty : false}
    },
    postcode: {
        presence: {allowEmpty : false}
    },
    email: {
        email: true,
        presence: {allowEmpty : false},
        length: {
            maximum: 20,
        }
    }
  };


const validation = (attributes) => {


   return new Promise((resolve,reject) => {
        validate.async(attributes, constraints).then(function(success){
            return resolve(undefined)
        },function(error){
            return resolve(error)
        })
        .catch(err => {
            return reject(err)
        });
    })
 
  
}

exports.default = validation