const express = require('express')
const app = express()
const bodyParser = require('body-parser')


const UserRouter = require('./router/user').default;


app.use(bodyParser.json())


app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use('/user',UserRouter)



app.listen(8080)