const express = require('express')
const app = express()
const bodyParser = require('body-parser')


const UserRouter = require('../router/user').default;


app.use(bodyParser.json())


app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use('/user',UserRouter)



const server = app.listen(8080,function(){
    var host = server.address().address;
    var port = server.address().port;

    console.log('App listening at http://%s:%s', host, port);
})



exports.default = server